require 'tmpdir'
require 'stringio'
require 'timeout'

LINDA=ENV['LINDA']||'linda'
$linda_runs = 0;

RSpec.describe 'Linda' do
  let(:home){ Dir.mktmpdir }
  let(:dat){ "#{home}/.linda/linda.dat" }
  let(:straceout){ "#{home}/strace.out" }
  let(:threads){ [] }
  before do
    ENV['HOME'] = home
  end

  after do
    join_threads()
  end

  def linda(opt)
    Timeout.timeout(10) {
      runs = $linda_runs+=1
      start = Time.now
      return {
        stdout: `strace -o #{straceout}.#{runs} -e trace=file #{LINDA} #{opt}`,
        status: $?.exitstatus,
        opens: File.readlines(straceout+".#{runs}").select do|i|
          i.include?('linda.dat') && i.include?('open')
        end.size,
        spend: Time.now - start,
      }
    }
  end

  def after(sec, opt)
    threads << Thread.new do
      sleep sec
      linda(opt)
    end
  end

  def join_threads()
    threads.each do |t|
      t.join
    end
  end

  describe 'setup' do
    it 'tmpdir' do
      expect(Dir.exists? home).to be true
    end
    it 'env' do
      expect(`env`).to include("HOME=#{home}")
    end
  end

  describe 'singletasks' do
    describe 'out' do
      it 'out' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(File.readlines(dat)).to be == ["hoge\n"]
      end
      it 'out2' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('out fuga')).to include(
          :stdout=>"", :status=>0)
        expect(File.readlines(dat)).to be == ["hoge\n","fuga\n"]
      end
    end
    describe 'rd' do
      it 'rd' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('rd hoge')).to include(
          :stdout=>"hoge\n", :status=>0)
        expect(File.readlines(dat)).to be == ["hoge\n"]
      end
      it 'rd with partial' do
        expect(linda('out abcdefg')).to include(
          :stdout=>"", :status=>0)
        expect(linda('rd -p cde')).to include(
          :stdout=>"", :status=>1)
        expect(File.readlines(dat)).to be == ["abcdefg\n"]
      end
      it 'rd with regexp' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('rd ho.*')).to include(
          :stdout=>"hoge\n", :status=>0)
        expect(File.readlines(dat)).to include("hoge\n")
      end
      it 'rdp with regexp' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('rdp ho.*')).to include(
          :stdout=>"hoge\n", :status=>0)
        expect(File.readlines(dat)).to include("hoge\n")
      end
      it 'rd -p with regexp' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('rd -p ho.*')).to include(
          :stdout=>"hoge\n", :status=>0)
        expect(File.readlines(dat)).to include("hoge\n")
      end
      it 'rdp with regexp not match' do
        expect(linda('out hoge')).to  include(
          :stdout=>"", :status=>0)
        expect(linda('rdp fu.*')).to include(
          :stdout=>"", :status=>1)
        expect(File.readlines(dat)).to include("hoge\n")
      end
      it 'rd -p with regexp not match' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('rd -p fu.*')).to include(
          :stdout=>"", :status=>1)
        expect(File.readlines(dat)).to include("hoge\n")
      end
      it 'rd -p with regexp not exist' do
        expect(linda('rd -p fu.*')).to include(
          :stdout=>"", :status=>1)
        expect(File.readlines(dat)).to be == []
      end
      it 'rd -p with backtrack', :no_bash, :no_golang do
        expect(linda('out hogehogehoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('rd -p "hoge(?!hogehoge$).*"')).to include(
          :stdout=>"", :status=>1)
        expect(linda('rd -p "hoge(?!fugafuga$).*"')).to include(
          :stdout=>"hogehogehoge\n", :status=>0)
        expect(File.readlines(dat)).to include("hogehogehoge\n")
      end
    end
    describe 'in' do
      it 'in' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('in hoge')).to include(
          :stdout=>"hoge\n", :status=>0)
        expect(File.readlines(dat)).to_not include("hoge\n")
      end
      it 'in2' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('in hoge')).to include(
          :stdout=>"hoge\n", :status=>0)
        expect(File.readlines(dat)).to be == ["hoge\n"]
      end
      it 'in with partial' do
        expect(linda('out abcdefg')).to include(
          :stdout=>"", :status=>0)
        expect(linda('in -p cde')).to include(
          :stdout=>"", :status=>1)
        expect(File.readlines(dat)).to be == ["abcdefg\n"]
      end
      it 'in with regexp' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('in ho.*')).to include(
          :stdout=>"hoge\n", :status=>0)
        expect(File.readlines(dat)).to be == []
      end
      it 'in2 with regexp' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('in ho.*')).to include(
          :stdout=>"hoge\n", :status=>0)
        expect(File.readlines(dat)).to be == ["hoge\n"]
      end
      it 'inp with regexp' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('inp ho.*')).to include(
          :stdout=>"hoge\n", :status=>0)
        expect(File.readlines(dat)).to be == []
      end
      it 'in -p with regexp' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('in -p ho.*')).to include(
          :stdout=>"hoge\n", :status=>0)
        expect(File.readlines(dat)).to be == []
      end
      it 'inp with regexp not match' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('inp fu.*')).to include(
          :stdout=>"", :status=>1)
        expect(File.readlines(dat)).to be == ["hoge\n"]
      end
      it 'in -p with regexp not match' do
        expect(linda('out hoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('in -p fu.*')).to include(
          :stdout=>"", :status=>1)
        expect(File.readlines(dat)).to be == ["hoge\n"]
      end
      it 'in -p with regexp not exist' do
        expect(linda('in -p fu.*')).to include(
          :stdout=>"", :status=>1)
        expect(File.readlines(dat)).to be == []
      end
      it 'in -p with backtrack', :no_bash, :no_golang do
        expect(linda('out hogehogehoge')).to include(
          :stdout=>"", :status=>0)
        expect(linda('in -p "hoge(?!hogehoge$).*"')).to include(
          :stdout=>"", :status=>1)
        expect(linda('in -p "hoge(?!fugafuga$).*"')).to include(
          :stdout=>"hogehogehoge\n", :status=>0)
        expect(File.readlines(dat)).to be == []
      end
    end
  end

  describe 'multitasks' do
    describe 'rd' do
      it 'rd-out' do
        after(0.1,'out hoge')
        expect(linda('rd hoge')).to include(
          :stdout=>"hoge\n",:status=>0,:spend=>be > 0.1)
        expect(File.readlines(dat)).to include("hoge\n")
      end
      it 'rd with regexp - out' do
        after(0.1,'out hoge')
        expect(linda('rd ho.*')).to include(
          :stdout=>"hoge\n",:status=>0,:spend=>be > 0.1)
        expect(File.readlines(dat)).to include("hoge\n")
      end
    end
    describe 'in' do
      it 'in-out' do
        after(0.1,'out hoge')
        expect(linda('in hoge')).to include(
          :stdout=>"hoge\n",:status=>0,:spend=>be > 0.1)
        expect(File.readlines(dat)).to_not include("hoge\n")
      end
      it 'in with regexp - out' do
        after(0.1,'out hoge')
        expect(linda('in ho.*')).to include(
          :stdout=>"hoge\n",:status=>0,:spend=>be > 0.1)
        expect(File.readlines(dat)).to_not include("hoge\n")
      end
      it 'in-in-out-out' do
        after(0.1,'out hoge')
        after(0.3,'out hoge')
        expect(linda('in hoge')).to include(
          :stdout=>"hoge\n",:status=>0,:spend=>be > 0.1)
        expect(linda('in hoge')).to include(
          :stdout=>"hoge\n",:status=>0,:spend=>be > 0.1)
        expect(File.readlines(dat)).to_not include("hoge\n")
      end
    end
  end
  describe 'regression' do
    it 'in-in-out-out' do
      after(0.1, 'in hoge')
      after(0.2, 'out hoge')
      after(0.3, 'out fuga')
      expect(linda('in fuga')).to include(
          :stdout=>"fuga\n",:status=>0)
    end
    it 'in-out-without-busyloop' do
      after(0.1, 'out fuga')
      expect(linda('in fuga')).to include(
          :stdout=>"fuga\n",:status=>0, :opens=>be <= 20)
    end
  end
end

