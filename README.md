# Linda command

A command line tool based on the idea of [coordination language Linda][wikipedia]

[wikipedia]: https://en.wikipedia.org/wiki/Linda_(coordination_language)

# Usage

```
linda out hogehoge
```
```out``` subcommand puts data hogehoge to linda space.

```
linda rd hogehoge
linda rd 'hoge.*'
linda rdp hogehoge
linda rd -p hogehoge
```
```rd``` subcommand reads data from linda space.
You can specify the data to read by regex.
If there are no data match to the argument,
this command will wait until the data is fullfilled.
```rdp``` command or ```-p``` is unblocking version of ```rd```.
This command will exit immediately with errorcode 1
if there are no such data.

```
linda in hogehoge
linda in 'hoge.*'
linda inp hogehoge
linda in -p hogehoge
```
Take out data from linda space.
Similar to ```rd```, but ```in``` subcommand also remove the data from space.


# Features

| Impl    | compile | test | out | in | rd | blocking | inotify | unix domain socket |
|---------|---------|------|-----|----|----|----------|---------|--------------------|
| scheme  | ✓       | ✓    | ✓   | ✓  | ✓  | ✓        | 🤨      | 🤨                 |
| newlisp | ✓       | ✓    | ✓   | ✓  | ✓  | ✓        | 🤨      | 🤨                 |
| bash    | ✓       | ✓    | ✓   | ✓  | ✓  | ✓        | 🤨      | ✓                  |
| dlang   | ✓       | ✓    | ✓   | ✓  | ✓  | ✓        | ✓       | 🤨                 |
| C++     | ✓       | ✓    | ✓   | ✓  | ✓  | ✓        | ✓       | ✓                  |
| Rust    | ✓       | ✓    | ✓   | ✓  | ✓  | ✓        | ✓       | ✓                  |
| Golang  | ✓       | ✓    | ✓   | ✓  | ✓  | ✓        | 🤨       | ✓                  |

# Limitation

## bash

- Bash version doesn't support advanced regex like backtracking.
- Bash version requires netcat-openbsd, grep, sed.

# Size & compile time

|Impl              |Bin size|lib size|compile time|
|------------------|--------|--------|------------|
|chicken scheme    |165k    |5.0M    |            |
|newlisp           |390k    |        |            |
|dlang(gdc)        |14M     |        |2.70 sec    |
|dlang(ldc)        |1.5M    |5.7M    |1.55 sec    |
|dlang(dmd)        |3.3M    |        |2.59 sec    |
|C++(gcc)          |512k    |        |2.06 sec    |
|C++(clang)        |551k    |        |1.28 sec    |
|Rust              |912k    |        |0.94 sec    |
|golang            |3.1M    |        |0.40 sec    |

## Scheme (Chicken scheme)

```
% ls -alh
total 5.0M
drwxrwxr-x 2 ikeji ikeji 4.0K  1月 27 13:20 .
drwxrwxr-x 3 ikeji ikeji 4.0K  1月 27 13:20 ..
-rw-r--r-- 1 ikeji ikeji 4.3M  1月 27 13:19 libchicken.so.8
-rwxrwxr-x 1 ikeji ikeji 165K  1月 27 13:19 linda
-rwxrwxr-x 1 ikeji ikeji  29K  1月 27 13:20 posix-extras.import.so
-rw-rw-r-- 1 ikeji ikeji   99  1月 27 13:20 posix-extras.setup-info
-rwxrwxr-x 1 ikeji ikeji 157K  1月 27 13:20 posix-extras.so
-rwxrwxr-x 1 ikeji ikeji  25K  1月 27 13:20 regex.import.so
-rw-rw-r-- 1 ikeji ikeji   72  1月 27 13:20 regex.setup-info
-rwxrwxr-x 1 ikeji ikeji 162K  1月 27 13:20 regex.so
-rwxrwxr-x 1 ikeji ikeji  25K  1月 27 13:20 srfi-37.import.so
-rw-rw-r-- 1 ikeji ikeji  114  1月 27 13:20 srfi-37.setup-info
-rwxrwxr-x 1 ikeji ikeji 133K  1月 27 13:20 srfi-37.so
```

## newlisp

```
% ls -alh linda
-rwxr-xr-x 1 ikeji ikeji 390K  1月 27 09:18 linda
```

## dlang (gdc)

```
% ls -alh linda
-rwxrwxr-x 1 ikeji ikeji 14M  1月 27 13:25 linda
```

```
% perf stat -r 100 -d /usr/bin/gdc linda.d

 Performance counter stats for '/usr/bin/gdc linda.d' (100 runs):

       2696.507386      task-clock (msec)         #    0.999 CPUs utilized            ( +-  1.04% )
                27      context-switches          #    0.010 K/sec                    ( +-  6.35% )
                 1      cpu-migrations            #    0.001 K/sec                    ( +-  9.66% )
           130,880      page-faults               #    0.049 M/sec                    ( +-  0.00% )
     9,207,605,708      cycles                    #    3.415 GHz                      ( +-  0.19% )  (49.94%)
    13,176,900,241      instructions              #    1.43  insn per cycle           ( +-  0.08% )  (62.50%)
     2,900,340,384      branches                  # 1075.591 M/sec                    ( +-  0.05% )  (75.03%)
        66,251,062      branch-misses             #    2.28% of all branches          ( +-  0.08% )  (75.09%)
     3,441,443,175      L1-dcache-loads           # 1276.260 M/sec                    ( +-  0.07% )  (72.31%)
       160,561,606      L1-dcache-load-misses     #    4.67% of all L1-dcache hits    ( +-  0.35% )  (25.01%)
        48,791,289      LLC-loads                 #   18.094 M/sec                    ( +-  0.49% )  (25.09%)
         7,458,814      LLC-load-misses           #   15.29% of all LL-cache hits     ( +-  1.39% )  (37.51%)

       2.697900564 seconds time elapsed                                          ( +-  1.04% )
```

## dlang (ldc)

```
% ls -alh linda
-rwxrwxr-x 1 ikeji ikeji 1.5M  1月 27 13:33 linda
% ldd linda
        linux-vdso.so.1 =>  (0x00007ffe4faa6000)
        libphobos2-ldc.so.74 => /usr/lib/x86_64-linux-gnu/libphobos2-ldc.so.74 (0x00007f5135ec5000)
        libdruntime-ldc.so.74 => /usr/lib/x86_64-linux-gnu/libdruntime-ldc.so.74 (0x00007f5135bb5000)
        librt.so.1 => /lib/x86_64-linux-gnu/librt.so.1 (0x00007f51359ad000)
        libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f513578e000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f51353ae000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f5136845000)
        libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f5135058000)
        libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f5134e54000)
        libz.so.1 => /lib/x86_64-linux-gnu/libz.so.1 (0x00007f5134c37000)
        libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f5134a20000)
% ls -alh /usr/lib/x86_64-linux-gnu/libphobos2-ldc.so.2.0.74 /usr/lib/x86_64-linux-gnu/libdruntime-ldc.so.2.0.74 
-rw-r--r-- 1 root root 1.1M  9月 30 06:36 /usr/lib/x86_64-linux-gnu/libdruntime-ldc.so.2.0.74
-rw-r--r-- 1 root root 4.6M  9月 30 06:36 /usr/lib/x86_64-linux-gnu/libphobos2-ldc.so.2.0.74
```
```
% perf stat -r 100 -d /usr/bin/ldc2 linda.d

 Performance counter stats for '/usr/bin/ldc2 linda.d' (100 runs):

       1546.911231      task-clock (msec)         #    0.999 CPUs utilized            ( +-  0.95% )
                16      context-switches          #    0.010 K/sec                    ( +-  6.72% )
                 1      cpu-migrations            #    0.000 K/sec                    ( +- 14.93% )
           102,672      page-faults               #    0.066 M/sec                    ( +-  0.00% )
     5,451,406,762      cycles                    #    3.524 GHz                      ( +-  0.14% )  (50.08%)
     6,612,472,954      instructions              #    1.21  insn per cycle           ( +-  0.13% )  (62.59%)
     1,397,377,582      branches                  #  903.334 M/sec                    ( +-  0.09% )  (75.07%)
        40,146,265      branch-misses             #    2.87% of all branches          ( +-  0.10% )  (75.07%)
     1,799,985,890      L1-dcache-loads           # 1163.600 M/sec                    ( +-  0.07% )  (72.06%)
        92,686,220      L1-dcache-load-misses     #    5.15% of all L1-dcache hits    ( +-  0.54% )  (25.06%)
        36,144,083      LLC-loads                 #   23.365 M/sec                    ( +-  1.21% )  (25.14%)
         4,519,789      LLC-load-misses           #   12.50% of all LL-cache hits     ( +-  0.95% )  (37.63%)

       1.547886439 seconds time elapsed                                          ( +-  0.95% )
```

## dlang (dmd)

```
% ls -alh linda
-rwxrwxr-x 1 ikeji ikeji 3.3M  1月 27 13:39 linda
```
```
% perf stat -r 100 -d /usr/bin/dmd linda.d

 Performance counter stats for '/usr/bin/dmd linda.d' (100 runs):

       2257.492861      task-clock (msec)         #    0.999 CPUs utilized            ( +-  1.04% )
                21      context-switches          #    0.009 K/sec                    ( +-  8.70% )
                 1      cpu-migrations            #    0.000 K/sec                    ( +- 13.17% )
           153,392      page-faults               #    0.068 M/sec                    ( +-  0.00% )
     7,571,314,492      cycles                    #    3.354 GHz                      ( +-  0.21% )  (50.14%)
     9,579,929,665      instructions              #    1.27  insn per cycle           ( +-  0.14% )  (62.63%)
     1,936,989,004      branches                  #  858.027 M/sec                    ( +-  0.09% )  (75.09%)
        37,598,750      branch-misses             #    1.94% of all branches          ( +-  0.10% )  (75.07%)
     3,115,300,805      L1-dcache-loads           # 1379.983 M/sec                    ( +-  0.06% )  (72.84%)
       163,881,655      L1-dcache-load-misses     #    5.26% of all L1-dcache hits    ( +-  0.35% )  (25.07%)
        57,614,763      LLC-loads                 #   25.522 M/sec                    ( +-  0.87% )  (25.14%)
         8,560,314      LLC-load-misses           #   14.86% of all LL-cache hits     ( +-  1.62% )  (37.66%)

       2.258797837 seconds time elapsed                                          ( +-  1.05% )
```

## C++ (gcc)

```
% ls -alh linda
-rwxrwxr-x 1 ikeji ikeji 572K  1月 27 13:49 linda
```
```
% perf stat -r 100 -d /usr/bin/g++ linda.cpp

 Performance counter stats for '/usr/bin/g++ linda.cpp' (100 runs):

       2058.223784      task-clock (msec)         #    0.999 CPUs utilized            ( +-  0.90% )
                27      context-switches          #    0.013 K/sec                    ( +-  5.15% )
                 1      cpu-migrations            #    0.001 K/sec                    ( +-  9.20% )
            67,129      page-faults               #    0.033 M/sec                    ( +-  0.00% )
     6,804,114,591      cycles                    #    3.306 GHz                      ( +-  0.12% )  (49.83%)
     8,133,400,885      instructions              #    1.20  insn per cycle           ( +-  0.08% )  (62.43%)
     1,845,169,714      branches                  #  896.486 M/sec                    ( +-  0.05% )  (74.99%)
        53,029,256      branch-misses             #    2.87% of all branches          ( +-  0.06% )  (75.09%)
     2,129,670,515      L1-dcache-loads           # 1034.713 M/sec                    ( +-  0.06% )  (71.50%)
       105,878,418      L1-dcache-load-misses     #    4.97% of all L1-dcache hits    ( +-  0.25% )  (25.06%)
        43,476,971      LLC-loads                 #   21.124 M/sec                    ( +-  0.45% )  (25.04%)
         5,205,734      LLC-load-misses           #   11.97% of all LL-cache hits     ( +-  1.22% )  (37.42%)

       2.059422492 seconds time elapsed                                          ( +-  0.90% )
```

## C++ (clang)

```
% ls -alh linda
-rwxrwxr-x 1 ikeji ikeji 551K  1月 27 13:50 linda
```
```
% perf stat -r 100 -d /usr/bin/clang++ -std=gnu++11 linda.cpp

 Performance counter stats for '/usr/bin/clang++ -std=gnu++11 linda.cpp' (100 runs):

       1281.338255      task-clock (msec)         #    0.999 CPUs utilized            ( +-  0.88% )
                13      context-switches          #    0.010 K/sec                    ( +-  8.43% )
                 0      cpu-migrations            #    0.000 K/sec                    ( +- 16.61% )
            31,399      page-faults               #    0.025 M/sec                    ( +-  0.09% )
     4,458,355,342      cycles                    #    3.479 GHz                      ( +-  0.12% )  (50.31%)
     5,128,908,405      instructions              #    1.15  insn per cycle           ( +-  0.11% )  (62.69%)
     1,078,799,735      branches                  #  841.932 M/sec                    ( +-  0.07% )  (75.08%)
        25,472,201      branch-misses             #    2.36% of all branches          ( +-  0.10% )  (75.05%)
     1,371,997,001      L1-dcache-loads           # 1070.753 M/sec                    ( +-  0.07% )  (70.81%)
        66,103,805      L1-dcache-load-misses     #    4.82% of all L1-dcache hits    ( +-  0.57% )  (25.23%)
        34,012,433      LLC-loads                 #   26.544 M/sec                    ( +-  1.06% )  (25.44%)
         2,608,370      LLC-load-misses           #    7.67% of all LL-cache hits     ( +-  1.76% )  (37.96%)

       1.282067178 seconds time elapsed                                          ( +-  0.88% )
```

## Rust


```
$ ls -alh linda
-rwxrwxr-x 2 ikeji ikeji 912K  2月 10 01:01 linda
```

```
$ perf stat -r 100 -d rustc --crate-name linda src/main.rs --crate-type bin --emit=dep-info,link -C metadata=09bc8cac6952c8d6 -C extra-filename=-09bc8cac6952c8d6 --out-dir (snip)

 Performance counter stats for 'rustc --crate-name linda src/main.rs --crate-type bin --emit=dep-info,link -C metadata=09bc8cac6952c8d6 -C extra-filename=-09bc8cac6952c8d6 --out-dir (snip)

        944.445546      task-clock (msec)         #    1.008 CPUs utilized            ( +-  0.68% )
                45      context-switches          #    0.047 K/sec                    ( +-  3.01% )
                 1      cpu-migrations            #    0.001 K/sec                    ( +- 18.44% )
            32,448      page-faults               #    0.034 M/sec                    ( +-  0.01% )
     3,420,634,199      cycles                    #    3.622 GHz                      ( +-  0.19% )  (50.75%)
     4,266,197,421      instructions              #    1.25  insn per cycle           ( +-  0.24% )  (63.10%)
       849,221,390      branches                  #  899.175 M/sec                    ( +-  0.22% )  (62.61%)
        21,457,202      branch-misses             #    2.53% of all branches          ( +-  0.16% )  (62.56%)
     1,094,539,910      L1-dcache-loads           # 1158.923 M/sec                    ( +-  0.12% )  (57.38%)
        57,623,474      L1-dcache-load-misses     #    5.26% of all L1-dcache hits    ( +-  0.61% )  (25.59%)
        22,855,820      LLC-loads                 #   24.200 M/sec                    ( +-  0.65% )  (25.97%)
         2,522,145      LLC-load-misses           #   11.04% of all LL-cache hits     ( +-  1.51% )  (38.55%)

       0.937252264 seconds time elapsed   
```
