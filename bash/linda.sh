#!/usr/bin/bash

USE_SLEEP=
USE_UNIXDOMAINSOCKETS=1

if [ -z "$USE_UNIXDOMAINSOCKETS" -a -z "$USE_SLEEP" ]; then
  echo "Error: need setup."
  exit -1
fi

COMMAND=$1
NONBLOCKING=
TEXT=
if [ "$2" = "-p" ];then
  NONBLOCKING=1
  TEXT=$3
else
  TEXT=$2
fi

# echo COMMAND=$COMMAND
# echo NONBLOCKING=$NONBLOCKING
# echo TEXT=$TEXT

LINDA_HOME=${LINDA_HOME:-${HOME}/.linda}
LINDA_LOCK=$LINDA_HOME/linda.lock
LINDA_DATA=$LINDA_HOME/linda.dat
LINDA_TMP=$LINDA_HOME/linda.tmp
LINDA_TMP2=$LINDA_HOME/linda.tmp2
LINDA_WAIT=$LINDA_HOME/linda.wait.$$

mkdir -p $LINDA_HOME
if [ ! -e $LINDA_DATA ];then
  touch $LINDA_DATA
fi

# echo LINDA_HOME=$LINDA_HOME
# echo LINDA_LOCK=$LINDA_LOCK
# echo LINDA_DATA=$LINDA_DATA
# echo LINDA_TMP=$LINDA_TMP
# echo LINDA_TMP2=$LINDA_TMP2
# echo LINDA_WAIT=$LINDA_WAIT

out_command() {
  (
    flock 99
    echo $TEXT >> $LINDA_DATA
  ) 99>$LINDA_LOCK
  if [ ! -z "$USE_UNIXDOMAINSOCKETS" ]; then
    touch ${LINDA_HOME}/linda.wait.block
    for s in ${LINDA_HOME}/linda.wait.*
    do
      echo $TEXT | nc -U $s >& /dev/null
      if [ $? = 1 ]; then
        # Remove stale socket
        rm $s
      fi
    done
  fi
}

rd_command() {
  while true;
  do
    exec 99>$LINDA_LOCK
    flock 99
    cat $LINDA_DATA | grep -E "^$TEXT$" > $LINDA_TMP
    if [ -s $LINDA_TMP ]; then
      cat $LINDA_TMP
      exit 0
    fi
    if [ ! -z "$NONBLOCKING" ]; then
      exit 1
    fi
    # Unlock
    exec 99>&-
    if [ ! -z $USE_SLEEP ]; then
      sleep 0.1
    fi
    if [ ! -z $USE_UNIXDOMAINSOCKETS ]; then
      nc -lU $LINDA_WAIT | read n
      rm $LINDA_WAIT
    fi
  done
}

in_command() {
  while true;
  do
    exec 99>$LINDA_LOCK
    flock 99
    cat $LINDA_DATA | grep -E "^$TEXT$" | head -n 1 > $LINDA_TMP
    if [ -s $LINDA_TMP ]; then
      cat $LINDA_DATA | sed "0,/^${TEXT}$/{//d}" > $LINDA_TMP2
      mv $LINDA_TMP2 $LINDA_DATA
      cat $LINDA_TMP
      exit 0
    fi
    if [ ! -z "$NONBLOCKING" ]; then
      exit 1
    fi
    # Unlock
    exec 99>&-
    if [ ! -z "$USE_SLEEP" ]; then
      sleep 0.1
    fi
    if [ ! -z $USE_UNIXDOMAINSOCKETS ]; then
      nc -lU $LINDA_WAIT | read n
      rm $LINDA_WAIT
    fi
  done
}


case "$COMMAND" in
  "in")
    in_command
  ;;
  "inp")
    NONBLOCKING=1
    in_command
  ;;
  "rd")
    rd_command
  ;;
  "rdp")
    NONBLOCKING=1
    rd_command
  ;;
  "out")
    out_command
  ;;
  "*")
    echo Command error
    exit -1
  ;;
esac
