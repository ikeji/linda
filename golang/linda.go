package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"syscall"
	"time"
)

// const wait_mode = "SLEEP"
const wait_mode = "UNIX_DOMAIN_SOCKETS"

var workingDirectory string
var lockFile string
var dataFile string
var tmpFile string
var waitSocket string

func initLocs() {
	workingDirectory = os.Getenv("LINDA_HOME")
	if workingDirectory == "" {
		home := os.Getenv("HOME")
		if home == "" {
			panic("No HOME")
		}
		workingDirectory = home + "/.linda"
	}
	log.Printf("workingDirectory: %s\n", workingDirectory)
	lockFile = workingDirectory + "/linda.lock"
	log.Printf("lockFile: %s\n", lockFile)
	dataFile = workingDirectory + "/linda.dat"
	log.Printf("dataFile: %s\n", dataFile)
	tmpFile = workingDirectory + "/linda.tmp"
	log.Printf("tmpFile: %s\n", tmpFile)
	waitSocket = workingDirectory + "/linda.wait." + strconv.Itoa(os.Getpid())
	log.Printf("waitSocket: %s\n", waitSocket)

	err := os.MkdirAll(workingDirectory, os.ModePerm)
	if err != nil {
		panic(err)
	}
	ensureFileExists()
}

func parseArg() (string, bool, string) {
	args := os.Args
	if len(args) < 2 {
		panic("No command")
	}
	command := args[1]
	if command == "help" {
		return command, false, ""
	}
	if len(args) < 3 {
		panic("No data")
	}
	data := args[2]
	blocking := true
	if data == "-p" {
		blocking = false
		if len(args) < 4 {
			panic("No data")
		}
		data = args[3]
	}
	log.Printf("command: %s\n", command)
	log.Printf("blocking: %t\n", blocking)
	log.Printf("data: %s\n", data)
	return command, blocking, data
}

func lock(fn func() int) int {
	lock, err := os.OpenFile(lockFile, os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		panic(err)
	}
	defer lock.Close()
	err = syscall.Flock(int(lock.Fd()), syscall.LOCK_EX)
	if err != nil {
		panic(err)
	}
	return fn()
}

func ensureFileExists() {
	lock(func() int {
		f, err := os.OpenFile(dataFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		return 0
	})
}

func outCmd(data string) int {
	return lock(func() int {
		f, err := os.OpenFile(dataFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			panic(err)
		}
		_, err = f.WriteString(data + "\n")
		if err != nil {
			panic(err)
		}
		err = f.Close()
		if err != nil {
			panic(err)
		}
		files, err := filepath.Glob(workingDirectory + "/linda.wait.*")
		if err != nil {
			panic(err)
		}
		for _, file := range files {
			conn, err := net.Dial("unix", file)
			if err != nil {
				os.Remove(file)
				continue
			}
			_, err = conn.Write([]byte(data))
			if err != nil {
				panic(err)
			}
			conn.Close()
		}
		return 0
	})
}

const (
	SUCCESS = 0
	FAILED  = 1
	RETRY   = 2
)

func maybeWaitUntilSuccess(fn func() int) int {
	var l net.Listener
	if wait_mode == "UNIX_DOMAIN_SOCKETS" {
		log.Println("setup wait socket")
		var err error
		l, err = net.Listen("unix", waitSocket)
		if err != nil {
			panic(err)
		}
		remove := func() {
			log.Println("remove wait socket")
			l.Close()
			os.Remove(waitSocket)
		}
		defer remove()
	}
	for {
		rc := fn()
		if rc != RETRY {
			return rc
		}
		if wait_mode == "SLEEP" {
			time.Sleep(100 * time.Millisecond)
		}
		if wait_mode == "UNIX_DOMAIN_SOCKETS" {
			log.Println("wait socket")
			conn, err := l.Accept()
			log.Println("accept wait socket")
			if err != nil {
				panic(err)
			}
			defer conn.Close()
			buf := &bytes.Buffer{}
			_, err = io.Copy(buf, conn)
		}
	}
}

func inCmd(blocking bool, data string) int {
	ensureFileExists()
	r := regexp.MustCompile("\\A" + data + "\\z")
	return maybeWaitUntilSuccess(func() int {
		return lock(func() int {
			f, err := os.Open(dataFile)
			if err != nil {
				panic(err)
			}
			defer f.Close()
			s := bufio.NewScanner(f)
			found := false
			for s.Scan() {
				log.Printf("line: %s\n", s.Text())
				if r.MatchString(s.Text()) {
					fmt.Println(s.Text())
					log.Println("match")
					found = true
					break
				}
			}
			if !found {
				if blocking {
					return RETRY
				} else {
					return FAILED
				}
			}
			_, err = f.Seek(0, 0)
			if err != nil {
				panic(err)
			}
			w, err := os.OpenFile(tmpFile, os.O_CREATE|os.O_WRONLY, 0644)
			if err != nil {
				panic(err)
			}
			s = bufio.NewScanner(f)
			found = false
			for s.Scan() {
				if found || !r.MatchString(s.Text()) {
					if _, err = w.WriteString(s.Text() + "\n"); err != nil {
						panic(err)
					}
				} else {
					found = true
				}
			}
			err = w.Close()
			if err != nil {
				panic(err)
			}
			err = os.Rename(tmpFile, dataFile)
			if err != nil {
				panic(err)
			}
			return SUCCESS
		})
	})
}

func rdCmd(blocking bool, data string) int {
	ensureFileExists()
	r, err := regexp.Compile("\\A" + data + "\\z")
	if err != nil {
		panic(err)
	}
	return maybeWaitUntilSuccess(func() int {
		return lock(func() int {
			f, err := os.Open(dataFile)
			if err != nil {
				panic(err)
			}
			defer f.Close()
			s := bufio.NewScanner(f)
			for s.Scan() {
				log.Printf("line: %s\n", s.Text())
				if r.MatchString(s.Text()) {
					fmt.Println(s.Text())
					log.Println("match")
					return SUCCESS
				}
			}
			if blocking {
				return RETRY
			} else {
				return FAILED
			}
		})
	})
}

func main() {
	log.SetOutput(io.Discard)
	// log.SetOutput(os.Stderr)
	initLocs()
	command, blocking, data := parseArg()
	rv := 0
	switch command {
	case "out":
		rv = outCmd(data)
	case "in":
		rv = inCmd(blocking, data)
	case "inp":
		rv = inCmd(false, data)
	case "rd":
		rv = rdCmd(blocking, data)
	case "rdp":
		rv = rdCmd(false, data)
	case "help":
		fmt.Printf("Linda (golang+%s)\n", wait_mode)
		rv = 0
	default:
		panic("Unknown command")
	}
	os.Exit(rv)
}
