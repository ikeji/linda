#if !defined(USE_SLEEP) && !defined(USE_INOTIFY) && !defined(USE_UNIXDOMAINSOCKETS)
  #error "Need USE_SLEEP, USE_INOTIFY or USE_UNIXDOMAINSOCKETS"
#endif

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <limits.h>
#include <regex>
#include <string>
#include <sys/file.h>
#include <sys/inotify.h>
#include <sys/stat.h>
#include <unistd.h>
#include <memory>

#ifdef USE_UNIXDOMAINSOCKETS
#include <sys/socket.h>
#include <sys/un.h>
#include <dirent.h>
#endif

using namespace std;

unique_ptr<string> workingDirectory;
unique_ptr<string> lockFile;
unique_ptr<string> dataFile;
unique_ptr<string> tmpFile;
unique_ptr<string> waitSocket;

bool nonblocking = false;

void error(const string& msg) {
  cerr << msg << endl;
  exit(2);
}

void initializeFileLocations() {
  clog << "initializeFileLocations" << endl;
  if (const char* LINDA_HOME = getenv("LINDA_HOME")) {
    clog << "LINDA_HOME: " << LINDA_HOME << endl;
    workingDirectory = make_unique<string>(LINDA_HOME);
  } else if (const char* HOME = getenv("HOME")) {
    clog << "HOME: " << HOME << endl;
    workingDirectory = make_unique<string>(string(HOME)+"/.linda");
  } else {
    error("Couldn't find HOME");
  }
  clog << "workingDirectory: " << *workingDirectory << endl;
  lockFile = make_unique<string>(*workingDirectory + "/linda.lock");
  clog << "lockFile: " << *lockFile << endl;
  dataFile = make_unique<string>(*workingDirectory + "/linda.dat");
  clog << "dataFile: " << *dataFile << endl;
  tmpFile = make_unique<string>(*workingDirectory + "/linda.tmp");
  clog << "tmpFile: " << *tmpFile << endl;
  waitSocket = make_unique<string>(*workingDirectory + "/linda.wait." + to_string(getpid()));
  clog << "waitSocket: " << *waitSocket<< endl;
}

void parseArguments(int argc, const char** argv, string* command, string* data) {
  while (true) {
    int opt = getopt(argc, const_cast<char**>(argv), "vp");
    clog << "opt: " << opt << endl;
    if (opt == -1) break;
    clog << "opt: " << static_cast<char>(opt) << endl;
    switch (opt) {
      case 'v':
        clog.clear();
        break;
      case 'p':
        nonblocking = true;
        break;
      default:
        clog << "argc: " << argc << endl;
        for (int i=0;i<argc;i++) {
          clog << "argv(" << i << "): " << argv[i] << endl;
        }
        clog << "Unknown param at " << optind-1 << " which is " << argv[optind-1] << endl;
        error("Usage linda [-v] [-p] (out|in|inp|rd|rdp) data");
        break;
    }
  }
  clog << "optind: " << optind << endl;
  if (argc < optind + 1) {
    error("Needs commands");
  }
  if (argc < optind + 2) {
    error("Needs data");
  }
  *command = argv[optind];
  clog << "command: " << *command << endl;
  *data = argv[optind+1];
  clog << "data: " << *data << endl;
  clog << "nonblocking: " << (nonblocking?"true":"false") << endl;
}

class Lock {
 public:
  Lock() {
    fd = open(lockFile->c_str(), O_WRONLY | O_CREAT, 0777);
    if (fd == -1) error("Failed to open lock");
    int r = flock(fd, LOCK_EX);
    if (r != 0) error("Failed to get lock");
    clog << "lock accuired" << endl;
  }
  ~Lock() {
    close(fd);
    clog << "lock released" << endl;
  }
 private:
  int fd;
};

void prepareRepository() {
   mkdir(workingDirectory->c_str(), 0777);
   Lock l;
   ofstream dat(*dataFile, ofstream::app);
}

int outCommand(const string& text) {
  clog << "outCommand" << endl;
  prepareRepository();
  Lock l;
  ofstream dat(*dataFile, ofstream::app);
  dat << text << endl;
  dat.close();
  clog << "write: " << text << endl;
#ifdef USE_UNIXDOMAINSOCKETS
  clog << "use unixdomainsocket" << endl;
  clog << "Scan dir" << endl;
  DIR* dd = opendir(workingDirectory->c_str());
  while(true) {
    struct dirent* dent = readdir(dd);
    if (dent == NULL) break;
    clog << dent->d_name << endl;
    const char* waitPrefix = "linda.wait.";
    if (strncmp(dent->d_name, waitPrefix, sizeof(waitPrefix)) != 0) {
      clog << "It's NOT wait socket" << endl;
      continue;
    }
    clog << "It's wait socket." << endl;
    int sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sock == -1){
      perror("socket");
      error("failed to create socket");
    }
    struct sockaddr_un sa = {0};
    sa.sun_family = AF_UNIX;
    strcpy(sa.sun_path, (*workingDirectory + "/" + dent->d_name).c_str());
    clog << "target: " << sa.sun_path << endl;
    if (connect(sock, (struct sockaddr*) &sa, sizeof(struct sockaddr_un)) == -1) {
      // Dead socket, remove.
      close(sock);
      remove(sa.sun_path);
      continue;
    }
    if (write(sock, text.c_str(), text.size()) == -1) {
      // Dead socket, remove.
      perror("write");
      close(sock);
      remove(sa.sun_path);
      continue;
    }
    close(sock);
  }
  closedir(dd);
#endif
  return 0;
}

template <typename F>
int maybeWaitUntilSuccess(F fn) {
#ifdef USE_INOTIFY
  int fd = inotify_init();
  if (fd == -1) error("failed inotify_init");
  if (-1 == inotify_add_watch(fd, workingDirectory->c_str(), IN_MODIFY)) {
    error("failed inotify_add_watch");
  }
#endif
#ifdef USE_UNIXDOMAINSOCKETS
  int sock = socket(AF_UNIX, SOCK_STREAM, 0);
  if (sock == -1){
    perror("socket");
    error("failed to create socket");
  }
  struct sockaddr_un sa = {0};
  sa.sun_family = AF_UNIX;
  strcpy(sa.sun_path, waitSocket->c_str());
  remove(sa.sun_path);
  if (bind(sock, (struct sockaddr*)&sa, sizeof(struct sockaddr_un)) == -1){
    perror("bind");
    close(sock);
    remove(sa.sun_path);
    error("Failed to bind");
  }
  if (listen(sock, 128) == -1){
    perror("listen");
    close(sock);
    remove(sa.sun_path);
    error("Failed to listen");
  }
#endif
  int ret;
  while (true) {
    ret = fn();
    if (ret == 0 || nonblocking) break;
    clog << "sleep" << endl;
#ifdef USE_INOTIFY
    clog << "use inotify" << endl;
    char buf[sizeof(inotify_event) + NAME_MAX + 1];
    auto bytes = read(fd, buf, sizeof(buf));
    if (bytes == 0) error("read failed");
#endif
#ifdef USE_SLEEP
    clog << "use sleep" << endl;
    usleep(10000);
#endif
#ifdef USE_UNIXDOMAINSOCKETS
    clog << "use unixdomainsocket" << endl;
    int fd = accept(sock, NULL, NULL);
    if (fd == -1) {
      perror("accept");
      close(sock);
      remove(sa.sun_path);
      error("Error on accept");
    }
    char buf[4096];
    int recv_size = read(fd, buf, sizeof(buf) -1);
    if (recv_size == -1) {
      perror("read");
      close(fd);
      close(sock);
      remove(sa.sun_path);
      error("Error on read");
    }
    buf[recv_size] = '\0';
    if (close(fd) == -1) {
      perror("close");
      close(sock);
      remove(sa.sun_path);
      error("Error on close");
    }
    clog << "receive " << buf << endl;
#endif
    clog << "wake up" << endl;
  }
#ifdef USE_INOTIFY
  close(fd);
#endif
#ifdef USE_UNIXDOMAINSOCKETS
  close(sock);
  remove(sa.sun_path);
#endif
  return ret;
}

int rdCommand(string pattern) {
  clog << "rdCommand" << endl;
  regex re(pattern);
  prepareRepository();
  return maybeWaitUntilSuccess([re](){
    Lock l;
    ifstream dat(*dataFile);
    string line;
    while(getline(dat, line)) {
      clog << "line: " << line << endl;
      if (regex_match(line, re)) {
        clog << "found" << endl;
        cout << line << endl;
        return 0;
      }
    }
    clog << "not found" << endl;
    return 1;
  });
}

int inCommand(string pattern) {
  clog << "inCommand" << endl;
  regex re(pattern);
  prepareRepository();
  return maybeWaitUntilSuccess([re](){
    Lock l;
    ifstream dat(*dataFile);
    bool found = false;
    string line;
    while(getline(dat, line)) {
      clog << "line: " << line << endl;
      if (!found && regex_match(line, re)) {
        clog << "match" << endl;
        cout << line << endl;
        found = true;
        break;
      } else {
        clog << "not match" << endl;
      }
    }
    if (!found) return 1;
    dat.seekg(0, dat.beg);
    ofstream tmp(*tmpFile);
    found = false;
    while(getline(dat, line)) {
      clog << "line: " << line << endl;
      if (!found && regex_match(line, re)) {
        clog << "match" << endl;
        found = true;
      } else {
        clog << "not match" << endl;
        tmp << line << endl;
      }
    }
    tmp.close();
    rename(tmpFile->c_str(), dataFile->c_str());
    clog << "commit" << endl;
    return 0;
  });
}

int main(int argc, const char** argv) {
  clog.setstate(ios_base::failbit);
  string command;
  string data;
  parseArguments(argc, argv, &command, &data);
  initializeFileLocations();
  clog << "linda" << endl;
  int rv = 2;
  if (command == "out") {
    rv = outCommand(data);
  } else if (command == "rd") {
    rv = rdCommand(data);
  } else if (command == "rdp") {
    nonblocking = true;
    rv = rdCommand(data);
  } else if (command == "in") {
    rv = inCommand(data);
  } else if (command == "inp") {
    nonblocking = true;
    rv = inCommand(data);
  } else {
    error("Unknown command");
  }
  return rv;
}
