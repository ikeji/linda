test: test-newlisp test-d test-scheme test-cpp test-rust test-bash test-golang

test-newlisp:
	make -C newlisp
	LINDA=./newlisp/linda rspec --tag ~no_newlisp

test-d:
	make -C d
	LINDA=./d/linda rspec --tag ~no_d
	LINDA=./d/linda-sleep rspec --tag ~no_d

test-scheme:
	make -C scheme
	LINDA=./scheme/linda rspec --tag ~no_scheme

test-cpp:
	make -C cpp
	LINDA=./cpp/linda rspec --tag ~no_cpp
	LINDA='valgrind -q --error-exitcode=3 --leak-check=full ./cpp/linda' \
				rspec --tag ~no_cpp
	LINDA=./cpp/linda-sleep rspec --tag ~no_cpp
	LINDA='valgrind -q --error-exitcode=3 --leak-check=full ./cpp/linda-sleep' \
				rspec --tag ~no_cpp
	LINDA=./cpp/linda-inotify rspec --tag ~no_cpp
	LINDA='valgrind -q --error-exitcode=3 --leak-check=full ./cpp/linda-inotify' \
				rspec --tag ~no_cpp

test-rust:
	make -C rust
	LINDA=./rust/linda rspec --tag ~no_rust
	LINDA=./rust/linda-inotify rspec --tag ~no_rust

test-bash:
	make -C bash
	LINDA=./bash/linda rspec --tag ~no_bash
	LINDA=./bash/linda-sleep rspec --tag ~no_bash

test-golang:
	make -C golang
	LINDA=./golang/linda rspec --tag ~no_golang

clean:
	make -C newlisp clean
	make -C d clean
	make -C scheme clean
	make -C cpp clean
	make -C rust clean
	make -C golang clean
